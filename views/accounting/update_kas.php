<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Kas;

$kas = Kas::findOne(Yii::$app->request->get()['id']);
list($tahun, $bulan, $tanggal) = explode("-", $kas['tanggal']);
$tipe = "";
if(intval($kas['tipe']) == 1){
	$tipe = "Income";
}else{
	$tipe = "Outcome";
}

?>
<div class="col-md-12">
    <div class="widget widget-green">
        <div class="widget-title">
            <div class="widget-controls">
				<a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
				<a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
			</div>
            <h3><i class="fa fa-ok-circle"></i>Update kas</h3>
        </div>
			<div class="widget-content">
				<div class="row">
					<div class="col-md-12">
						
						<?php $form = ActiveForm::begin([
							'id' => 'create-role-form',
							'options' => ['class' => 'form-horizontal', 'data-toggle'=>'validator', 'role'=>'form'],
							'fieldConfig' => [
							'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
							'labelOptions' => ['class' => 'col-lg-1 control-label'],
							],
						]); ?>
                      		<div class="form-group">
				                <label>Type</label>
				                <select class="form-control" name="tipe" required = "required">
			                        <option value = "<?=$kas['tipe']?>"><?=$tipe?></option>
			                        <option value = "1">Income</option>
			                        <option value = "0">Outcome</option>
			                   	</select>
				           	</div>
							<div class="form-group">
				                <label>Quantity</label>
				                <input type="number" value = "<?=$kas['jumlah']?>"class="form-control" placeholder="" name="jumlah" required = "required">
				           	</div>

				           	<div class="form-group">
				                <label>Date</label>
				                <input type="text" value = "<?="$bulan/$tanggal/$tahun"?>" placeholder="" class="form-control input-datepicker" name="tanggal" required = "required">
				           	</div>
							
							<div class="form-group">
				                <label>Description</label>
				                <textarea class="form-control" placeholder="" name="deskripsi" required = "required">
				                	<?=$kas['deskripsi']?>
				                </textarea>
				           	</div>

							<?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>

						<?php ActiveForm::end(); ?>

					</div>
				</div>
			</div>
	</div>
</div>	