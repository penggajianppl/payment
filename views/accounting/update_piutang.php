<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Piutang;

$hutang = Piutang::findOne(Yii::$app->request->get()['id']);
list($tahun_hutang, $bulan_hutang, $tanggal_hutang) = explode("-", $hutang['tanggal_pinjam']);
list($tahun_tempo, $bulan_tempo, $tanggal_tempo) = explode("-", $hutang['jatuh_tempo']);
?>
<div class="col-md-12">
    <div class="widget widget-green">
        <div class="widget-title">
            <div class="widget-controls">
				<a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
				<a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
			</div>
            <h3><i class="fa fa-ok-circle"></i>update Piutang</h3>
        </div>
			<div class="widget-content">
				<div class="row">
					<div class="col-md-12">
						
						<?php $form = ActiveForm::begin([
							'id' => 'create-role-form',
							'options' => ['class' => 'form-horizontal', 'data-toggle'=>'validator', 'role'=>'form'],
							'fieldConfig' => [
							'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
							'labelOptions' => ['class' => 'col-lg-1 control-label'],
							],
						]); ?>

							<div class="form-group">
				                <label>Nama Peminjam</label>
				                <input type="text" class="form-control" placeholder="" value = "<?=$hutang['nama_peminjam']?>" name="nama_peminjam" required = "required">
				           	</div>

							<div class="form-group">
				                <label>Jumlah Piutang</label>
				                <input type="number" class="form-control" placeholder="" value = "<?=$hutang['jumlah_piutang']?>" name="jumlah_piutang" required = "required">
				           	</div>

				           	<div class="form-group">
				                <label>Tanggal Pinjam</label>
				                <input type="text" placeholder="" value = "<?="$bulan_hutang/$tanggal_hutang/$tahun_hutang"?>" class="form-control input-datepicker" name="tanggal_pinjam" required = "required">
				           	</div>

				           	<div class="form-group">
				                <label>Jatuh Tempo</label>
				                <input type="text" placeholder="" value = "<?="$bulan_tempo/$tanggal_tempo/$tahun_tempo"?>" class="form-control input-datepicker" name="jatuh_tempo" required = "required">
				           	</div>
							
							<div class="form-group">
				                <label>Description</label>
				                <textarea class="form-control" placeholder="" name="deskripsi" required = "required"><?=$hutang['deskripsi']?></textarea>
				           	</div>

							<?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>

						<?php ActiveForm::end(); ?>

					</div>
				</div>
			</div>
	</div>
</div>	