<?php
use yii\helpers\Html;

use app\models\TransaksiHp;

$cicilan = TransaksiHp::find()->where([
		'id_hutangpiutang'=>Yii::$app->request->get()['id'],
		'ket'=>1
	])->asArray()->all();
?>

<div class="col-md-12">
    <div class="widget widget-green">
        <div class="widget-title">
            <div class="widget-controls">
				<a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
				<a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
			</div>
            <h3><i class="fa fa-ok-circle"></i>list of cicilan piutang</h3>
        </div>
			<div class="widget-content">
				<div class="row">
					<div class="col-md-12">
						<a href="<?= Yii::$app->urlManager->createUrl(['accounting/add-cicilan-piutang','id'=>Yii::$app->request->get()['id']])?>" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i></a>
						<table class="table">
							<thead>
								<tr>
									<th>No</th>
									<th>Tanggal Transaksi</th>
									<th>Nominal</th>
									<th width="150px">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$i=1; foreach ( $cicilan as $key => $value) {
								?>
									<tr>
										<td><?=$i?></td>
										<td><?=$value['tgl_transaksi']?></td>
										<td><?=$value['nominal_dibayar']?></td>
										<td>
											
											<a href="<?= Yii::$app->urlManager->createUrl(['accounting/update-cicilan-piutang',"id_cicilan"=>$value['id_transaksi'], "id_piutang"=>Yii::$app->request->get()['id']])?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-pencil"></i></a>
											
											<a href="<?= Yii::$app->urlManager->createUrl(['accounting/delete-cicilan-piutang',"id_cicilan"=>$value['id_transaksi'], "id_piutang"=>Yii::$app->request->get()['id']])?>" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i></a>
										</td>
									</tr>
								<?php $i++; } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>