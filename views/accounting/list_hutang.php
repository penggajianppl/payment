<?php
use yii\helpers\Html;

use app\models\Hutang;
use app\models\TransaksiHp;

$kas = Hutang::find()->asArray()->all();
$transaksi = new TransaksiHp();
?>

<div class="col-md-12">
    <div class="widget widget-green">
        <div class="widget-title">
            <div class="widget-controls">
				<a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
				<a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
			</div>
            <h3><i class="fa fa-ok-circle"></i>list of Hutang</h3>
        </div>
			<div class="widget-content">
				<div class="row">
					<div class="col-md-12">
						<a href="<?= Yii::$app->urlManager->createUrl(['accounting/add-hutang'])?>" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i></a>
						<table class="table">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Pemberi Pinjaman</th>
									<th>Jumlah Hutang</th>
									<th>Sisa Hutang</th>
									<th>Tanggal Hutang</th>
									<th>Jatuh Tempo</th>
									<th>Deskripsi</th>
									<th width="150px">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$i=1; foreach ( $kas as $key => $value) {
								?>
									<tr>
										<td><?=$i?></td>
										<td><?=$value['nama_pemberi_pinjaman']?></td>
										<td><?=$value['jumlah_hutang']?></td>
										<td>
											<?php
												$dibayar = $transaksi->getTotalCicilanHutang($value['id_hutang']);
												if(!empty($dibayar)){
													echo (floatval($value['jumlah_hutang']) - floatval($dibayar['dibayar']));
												}else{
													echo $value['jumlah_hutang'];
												}
											?>
										</td>
										<td><?=$value['tanggal_hutang']?></td>
										<td><?=$value['jatuh_tempo']?></td>
										<td><?=$value['deskripsi']?></td>
										<td>
											
											<a href="<?= Yii::$app->urlManager->createUrl(['accounting/update-hutang',"id"=>$value['id_hutang']])?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-pencil"></i></a>
											
											<a href="<?= Yii::$app->urlManager->createUrl(['accounting/delete-hutang',"id"=>$value['id_hutang']])?>" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i></a>

											<a href="<?= Yii::$app->urlManager->createUrl(['accounting/cicilan-hutang',"id"=>$value['id_hutang']])?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-pencil"></i></a>
										</td>
									</tr>
								<?php $i++; } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>