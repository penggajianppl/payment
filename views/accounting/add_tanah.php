<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="col-md-12">
    <div class="widget widget-green">
        <div class="widget-title">
            <div class="widget-controls">
				<a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
				<a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
			</div>
            <h3><i class="fa fa-ok-circle"></i>Add new Tanah</h3>
        </div>
			<div class="widget-content">
				<div class="row">
					<div class="col-md-12">
						
						<?php $form = ActiveForm::begin([
							'id' => 'create-role-form',
							'options' => ['class' => 'form-horizontal', 'data-toggle'=>'validator', 'role'=>'form'],
							'fieldConfig' => [
							'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
							'labelOptions' => ['class' => 'col-lg-1 control-label'],
							],
						]); ?>
							<div class="form-group">
				                <label>No Tanah</label>
				                <input type="text" class="form-control" placeholder="" name="no_tanah" required = "required">
				           	</div>

				           	<div class="form-group">
				                <label>Luas Tanah (m2)</label>
				                <input type="text" class="form-control" placeholder="" name="luas" required = "required">
				           	</div>

				           	<div class="form-group">
				                <label>Harga Tanah</label>
				                <input type="number" class="form-control" placeholder="" name="harga" required = "required">
				           	</div>

							
							<div class="form-group">
				                <label>Lokasi</label>
				                <textarea class="form-control" placeholder="" name="lokasi" required = "required"></textarea>
				           	</div>

				           	<div class="form-group">
				                <label>Status</label>
				                <textarea class="form-control" placeholder="" name="status" required = "required"></textarea>
				           	</div>

				           	<div class="form-group">
				                <label>Deskripsi</label>
				                <textarea class="form-control" placeholder="" name="deskripsi" required = "required"></textarea>
				           	</div>

							<?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>

						<?php ActiveForm::end(); ?>

					</div>
				</div>
			</div>
	</div>
</div>	