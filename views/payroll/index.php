<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\payroll */
/* @var $form ActiveForm */
?>
<div class="payroll-index">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'employee_id') ?>
        <?= $form->field($model, 'employee_name') ?>
        <?= $form->field($model, 'salary') ?>
        <?= $form->field($model, 'pph') ?>
        <?= $form->field($model, 'jamkes') ?>
        <?= $form->field($model, 'other') ?>
        <?= $form->field($model, 'total_salary') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- payroll-index -->
