<?php
use yii\helpers\Html;

use app\models\Payroll;

$payroll = Payroll::find()->asArray()->all();
?>

<div class="col-md-12">
    <div class="widget widget-green">
        <div class="widget-title">
            <div class="widget-controls">
				<a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
				<a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
			</div>
            <h3><i class="fa fa-ok-circle"></i>list of employee</h3>
        </div>
			<div class="widget-content">
				<div class="row">
					<div class="col-md-12">
						<a href="<?= Yii::$app->urlManager->createUrl(['accounting/add-modal'])?>" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i></a>
						<table class="table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>salary</th>
									<th>PPH</th>
									<th>jamkes</th>
									<th>other</th>
									<th>total salary</th>
									<th width="150px">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $i=1; foreach ( $payroll as $key => $value) { ?>
									<tr>
										<!-- <td><?=$i?></td> -->
										<td><?=$value['employee_id']?></td>
										<td><?=$value['employee_name']?></td>
										<td><?=$value['salary']?></td>
										<td><?=$value['pph']?></td>
										<td><?=$value['jamkes']?></td>
										<td><?=$value['other']?></td>
										<td><?=$value['total_salary']?></td>
										<td>
											
											<a href="" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-cog"></i></a>
											
										</td>
									</tr>
								<?php $i++; } ?>
							</tbody>
						</table>

						<a href="" class="btn btn-primary btn-sm">Pengaturan Penggajian</a>
						<a href="" class="btn btn-primary btn-sm">Cetak Laporan</a>
					</div>
				</div>
			</div>
	</div>
</div>