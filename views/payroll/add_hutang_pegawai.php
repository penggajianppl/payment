<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Payroll;

$payroll = Payroll::find()->asArray()->all();
?>
<div class="col-md-12">
    <div class="widget widget-green">
        <div class="widget-title">
            <div class="widget-controls">
				<a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
				<a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
			</div>
            <h3><i class="fa fa-ok-circle"></i>Add hutang pegawai</h3>
        </div>
			<div class="widget-content">
				<div class="row">
					<div class="col-md-12">
						
						<?php $form = ActiveForm::begin([
							'id' => 'create-role-form',
							'options' => ['class' => 'form-horizontal', 'data-toggle'=>'validator', 'role'=>'form'],
							'fieldConfig' => [
							'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
							'labelOptions' => ['class' => 'col-lg-1 control-label'],
							],
						]); ?>
							<div class="form-group">
				                <label>Employee</label>
				                <select class="form‐control" name="employee_id" required = "required">
				                <?php $i=1; foreach ( $payroll as $key => $value) { ?>
									<option value="<?=$value['employee_id']?>"><?php
															echo $value['employee_id'] . " - " . $value['employee_name'];
															?>
									</option>
								<?php $i++; } ?>
								</select>
				           	</div>
							
							<div class="form-group">
				                <label>Jumlah Hutang (Rp.)</label>
				                <input type="number" class="form-control" placeholder="" name="jumlah_hutang" required = "required">
				           	</div>

							<?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>

						<?php ActiveForm::end(); ?>

					</div>
				</div>
			</div>
	</div>
</div>	