<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="col-md-12">
    <div class="widget widget-green">
        <div class="widget-title">
            <div class="widget-controls">
				<a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
				<a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
			</div>
            <h3><i class="fa fa-ok-circle"></i>Add employee payroll data</h3>
        </div>
			<div class="widget-content">
				<div class="row">
					<div class="col-md-12">
						
						<?php $form = ActiveForm::begin([
							'id' => 'create-role-form',
							'options' => ['class' => 'form-horizontal', 'data-toggle'=>'validator', 'role'=>'form'],
							'fieldConfig' => [
							'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
							'labelOptions' => ['class' => 'col-lg-1 control-label'],
							],
						]); ?>
							<div class="form-group">
				                <label>Employee ID</label>
				                <input type="number" class="form-control" placeholder="" name="employee_id" required = "required">
				           	</div>
							
							<div class="form-group">
				                <label>Nama Pegawai</label>
				                <input type="text" class="form-control" placeholder="" name="employee_name" required = "required">
				           	</div>

				           	<div class="form-group">
				                <label>Job Position</label>
				                <select class="form‐control" name="job_position" required = "required">
									<option value="1">administrator</option>
									<option value="2">manajer</option>
									<option value="3">HRD</option>
									<option value="4">IT Support</option>
									<option value="5">Karyawan</option>
								</select>
				           	</div>

							<?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>

						<?php ActiveForm::end(); ?>

					</div>
				</div>
			</div>
	</div>
</div>	