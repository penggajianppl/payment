<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hutang_pegawai".
 *
 * @property string $employee_id
 * @property double $hutang
 */
class HutangPegawai extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hutang_pegawai';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'hutang'], 'required'],
            [['hutang'], 'number'],
            [['cicilan'], 'number'],
            [['employee_id'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'employee_id' => 'Employee ID',
            'hutang' => 'Hutang',
            'cicilan' => 'cicilan',
        ];
    }
}
