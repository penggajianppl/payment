<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "job_position".
 *
 * @property integer $job_pos_id
 * @property string $job_pos_name
 */
class JobPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job_pos_id', 'job_pos_name'], 'required'],
            [['job_pos_id'], 'integer'],
            [['job_pos_name'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'job_pos_id' => 'Job Pos ID',
            'job_pos_name' => 'Job Pos Name',
        ];
    }
}
