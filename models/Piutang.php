<?php

namespace app\models;

use Yii;

class Piutang extends \yii\db\ActiveRecord{
	public static function tableName()
    {
        return 'piutang';
    }

    public function getHartaPiutang($from, $to){
    	$sql = 
    	"select sum(jumlah_piutang - coalesce(
		    (select sum(nominal_dibayar) as bayar 
		     from transakasi_hp 
		     where tgl_transaksi between '$from' and '$to'
		     group by id_hutangpiutang having id_hutangpiutang = id_piutang), 
		    0)) as piutang 
		from piutang 
		where tanggal_pinjam between '$from' and '$to'";
    	$model = self::findBySql($sql)->asArray()->one();
        return $model;	
    }
}
?>