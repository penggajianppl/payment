<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payroll".
 *
 * @property string $employee_id
 * @property string $employee_name
 * @property double $salary
 * @property double $pph
 * @property double $jamkes
 * @property double $other
 * @property double $total_salary
 */
class Payroll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'employee_name', 'salary', 'pph', 'jamkes', 'other', 'total_salary'], 'required'],
            [['salary', 'pph', 'jamkes', 'other', 'total_salary'], 'number'],
            [['employee_id'], 'string', 'max' => 10],
            [['employee_name'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'employee_id' => 'Employee ID',
            'employee_name' => 'Employee Name',
            'salary' => 'Salary',
            'pph' => 'Pph',
            'jamkes' => 'Jamkes',
            'other' => 'Other',
            'total_salary' => 'Total Salary',
        ];
    }
}
